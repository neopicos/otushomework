using System;
using System.Collections.Generic;
using Core.Domain.Administration;

namespace OtusWork.Models
{
    public class RoleItemResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }

    }
}