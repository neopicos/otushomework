using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Core.Abstractions;
using Core.Domain.Administration;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;
using OtusWork.Models;
using OtusWork.Models.Requests;

namespace OtusWork.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    FullName = x.FullName,
                    Email = x.Email
                }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }
        
        /// <summary>
        /// Удалить существующего сотрудника по Id
        /// </summary>
        [HttpDelete("{id:guid}")]
        public async Task<List<EmployeeShortResponse>> DeleteEmployeeAsync([Required] Guid id)
        {
            if (id == null)
            {
                throw new Exception("Param Id is null");
            }

            var employees = await _employeeRepository.DeleteAsync(id);
            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    FullName = x.FullName,
                    Email = x.Email
                }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Добавить нового сотрудника
        /// </summary>
        [HttpPost]
        public async Task<ActionResult> CreateEmployeeAsync(EmployeeRequest request)
        {
            if (request == null)
                return BadRequest();

            var oldEmployee = await _employeeRepository.GetByIdAsync(Guid.Parse(request.Id));
            if (oldEmployee != null)
                return StatusCode(409, $"User with Id = {request.Id} already exists ");

            var employee = new Employee()
            {
                Id = Guid.Parse(request.Id),
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email
            };

            var result = await _employeeRepository.AddOrUpdateAsync(employee);

            return StatusCode(result.Id != null ? 201 : 500);
        }
        
        /// <summary>
        /// Изменить данные существующего сотрудника
        /// </summary>
        [HttpPut]
        public async Task<ActionResult> UpdateEmployeeAsync(EmployeeRequest request)
        {
            if (request == null)
                return BadRequest("Param employee is null");

            var oldEmployee = await _employeeRepository.GetByIdAsync(Guid.Parse(request.Id));
            if (oldEmployee == null)
                return NotFound();

            var newEmployee = new Employee()
            {
                Id = Guid.Parse(request.Id),
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email
            };

            await _employeeRepository.AddOrUpdateAsync(newEmployee);

            return Ok();
        }
    }
}