using System;
using System.Collections;
using System.Threading.Tasks;
using Core.Domain;
using System.Collections.Generic;

namespace Core.Abstractions
{
    public interface IRepository<T> where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        Task<T> GetByIdAsync(Guid id);
        Task<IEnumerable<T>> DeleteAsync(Guid id);
        Task<T> AddOrUpdateAsync(T entity);
    }
}