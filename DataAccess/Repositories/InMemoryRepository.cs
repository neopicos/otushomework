using System;
using System.Collections;
using Core.Abstractions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Domain;

namespace DataAccess.Repositories
{
    public class InMemoryRepository<T> : IRepository<T> where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }
        
        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<IEnumerable<T>> DeleteAsync(Guid id)
        {
            Data = Data.Where(x => x.Id != id).ToList();
            return Task.FromResult(Data);
        }
        
        public Task<T> AddOrUpdateAsync(T entity)
        {
            var isExisted = Data.Any(x => x.Id == entity.Id);

            if (!isExisted)
            {
                Data = Data.Append(entity).ToList();
            }
            else
            {
                var tmpList = Data.Where(x => x.Id != entity.Id).ToList();
                tmpList.Add(entity);

                Data = tmpList;
            }

            return Task.FromResult(entity);
        }
    }
}